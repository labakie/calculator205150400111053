# Tugas 3 & Tugas 4 - Calculator

## Description

<p>This simple calculator was created to accomplish the tasks of mobile application programming on Android Studio. There are two branches in this repository:</p>
    <ol>
        <li>main branch for "tugas 3" with event handling as the main subject.</li>
        <li>intent-operation branch for "tugas 4" based on the main branch with a few added features.</li>
    </ol>

## Screenshot

<table>
    <tr>
        <td style="text-align: center">Tugas 3-Calculator Demo</td>
        <td style="text-align: center">Tugas 4-Calculator Demo</td>
    </tr>
    <tr>
        <td>![6vcsif](/uploads/d845fdd945fccc9eacc337b24beb9286/6vcsif.gif)</td>
        <td>![6vd1xu](/uploads/4473a9f7e1b6453252ebf6ee38bd2a56/6vd1xu.gif)</td>
    </tr>
</table>
