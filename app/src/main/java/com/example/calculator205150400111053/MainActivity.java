package com.example.calculator205150400111053;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView viewOperations;
    Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0,
            btMlt, btAdd, btSub, btDiv, btClear, btResult;

    public static double valueNow = 0;
    public static String operation = "";
    public static double resultOperations = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.orange)));

        viewOperations = (TextView)findViewById(R.id.tv2);
        viewOperations.setOnClickListener(this);
        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button) findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button) findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button) findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button) findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button) findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button) findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button) findViewById(R.id.bt9);
        bt9.setOnClickListener(this);
        bt0 = (Button) findViewById(R.id.bt0);
        bt0.setOnClickListener(this);

        btMlt = (Button) findViewById(R.id.bt_multiplication);
        btMlt.setOnClickListener(this);
        btAdd = (Button) findViewById(R.id.bt_addition);
        btAdd.setOnClickListener(this);
        btSub = (Button) findViewById(R.id.bt_substraction);
        btSub.setOnClickListener(this);
        btDiv = (Button) findViewById(R.id.bt_division);
        btDiv.setOnClickListener(this);
        btClear = (Button) findViewById(R.id.bt_clear);
        btClear.setOnClickListener(this);
        btResult = (Button) findViewById(R.id.bt_result);
        btResult.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt0:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("0"));
                break;
            case R.id.bt1:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("1"));
                break;
            case R.id.bt2:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("2"));
                break;
            case R.id.bt3:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("3"));
                break;
            case R.id.bt4:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("4"));
                break;
            case R.id.bt5:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("5"));
                break;
            case R.id.bt6:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("6"));
                break;
            case R.id.bt7:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("7"));
                break;
            case R.id.bt8:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("8"));
                break;
            case R.id.bt9:
                viewOperations.setText(viewOperations.getText().toString().trim() + ("9"));
                break;

            case R.id.bt_multiplication:
                if (viewOperations.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please fill the number for operation", Toast.LENGTH_SHORT).show();
                    return;
                }
                operation = "mlt";
                valueNow = Double.parseDouble(viewOperations.getText().toString());
                viewOperations.setText("");
                break;
            case R.id.bt_addition:
                if (viewOperations.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please fill the number for operation", Toast.LENGTH_SHORT).show();
                    return;
                }
                operation = "add";
                valueNow = Double.parseDouble(viewOperations.getText().toString());
                viewOperations.setText("");
                break;
            case R.id.bt_substraction:
                if (viewOperations.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please fill the number for operation", Toast.LENGTH_SHORT).show();
                    return;
                }
                operation = "sub";
                valueNow = Double.parseDouble(viewOperations.getText().toString());
                viewOperations.setText("");
                break;
            case R.id.bt_division:
                if (viewOperations.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "Please fill the number for operation", Toast.LENGTH_SHORT).show();
                    return;
                }
                operation = "div";
                valueNow = Double.parseDouble(viewOperations.getText().toString());
                viewOperations.setText("");
                break;
            case R.id.bt_clear:
                valueNow = 0;
                viewOperations.setText("");
                break;
            case R.id.bt_result:
                if (operation.equals("mlt")) {
                    resultOperations = valueNow * Double.parseDouble(viewOperations.getText().toString());
                }
                if (operation.equals("add")) {
                    resultOperations = valueNow + Double.parseDouble(viewOperations.getText().toString());
                }
                if (operation.equals("sub")) {
                    resultOperations = valueNow - Double.parseDouble(viewOperations.getText().toString());
                }
                if (operation.equals("div")) {
                    resultOperations = valueNow / Double.parseDouble(viewOperations.getText().toString());
                }

                int valueTemp = (int)resultOperations;

                if (valueTemp == resultOperations)
                    viewOperations.setText(String.valueOf((int)resultOperations));
                else
                    viewOperations.setText(String.valueOf(resultOperations));

                break;
        }
    }
}